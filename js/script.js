const button = document.getElementById("btn")
const information = document.getElementById("info");

button.addEventListener("click", async function() {
    try {
        
        const ipFetch = await fetch("https://api.ipify.org/?format=json");
        const ipData = await ipFetch.json();
        const clientIP = ipData.ip;
        console.log(clientIP);
        
        const phyAdress = await fetch(`https://ip-api.com/json/${clientIP}`);
        const createIndividual = await phyAdress.json();
        
        information.innerHTML = `
            <p>Континент: ${createIndividual.continent}</p>
            <p>Країна: ${createIndividual.country}</p>
            <p>Регіон: ${createIndividual.regionName}</p>
            <p>Місто: ${createIndividual.city}</p>
            <p>Район: ${createIndividual.district}</p>
        `;
    } catch (error) {
        console.error("Error:", error);
    }
});